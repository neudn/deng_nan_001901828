/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class VitalSignHistory {
   private ArrayList<VitalSign> vitalSignHistory;
   
   public VitalSignHistory(){
       vitalSignHistory = new ArrayList<VitalSign>();
   }

    public ArrayList<VitalSign> getVitalSignHistory() {
        return vitalSignHistory;
    }
   
    public VitalSign addVitalSign(){
        VitalSign p = new VitalSign();
        vitalSignHistory.add(p);
        return p;
    }
    
    public void deleteVitalSign(VitalSign p){
        vitalSignHistory.remove(p);
    }
    
    public VitalSign searchVitalSign(int i){
        for(VitalSign p: vitalSignHistory){
            if(p.getModelNumber() == i) return p;
        }
        return null;
    }
}
