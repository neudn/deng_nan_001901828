/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class PersonDirectory {
    
    private ArrayList<Person> personDirectory;
    
    public PersonDirectory(){
        personDirectory = new ArrayList<Person>();
    }

    public ArrayList<Person> getPersonDirectory() {
        return personDirectory;
    }

    public void setSupplierDirectory(ArrayList<Person> personDirectory) {
        this.personDirectory = personDirectory;
    }
    
    public Person addPerson(){
        Person s  = new Person();
        personDirectory.add(s);
        return s;
    }
    
    public void deletePerson(Person s){
        personDirectory.remove(s);
    }
    
    public Person searchPerson(String keyWord){
        for(Person s: personDirectory){
            if(s.getID().equalsIgnoreCase(keyWord)) return s;
        }
        return null;
    }
}

