/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Solution {
    public static void main(String[] args){
        SupplierDirectory sd = new SupplierDirectory();
        
        Supplier Lenovo = new Supplier();
        sd.getSupplierDirectory().add(Lenovo);
        ProductCatalog lenovoPC = new ProductCatalog();
        Product X1 = new Product("X1", 99); lenovoPC.getProductList().add(X1);
        Product X2 = new Product("X2", 199); lenovoPC.getProductList().add(X2);
        Product X3 = new Product("X3", 299); lenovoPC.getProductList().add(X3);
        Product X4 = new Product("X4", 399); lenovoPC.getProductList().add(X4);
        Product X5 = new Product("X5", 499); lenovoPC.getProductList().add(X5);
        Product X6 = new Product("X6", 599); lenovoPC.getProductList().add(X6);
        Product X7 = new Product("X7", 699); lenovoPC.getProductList().add(X7);
        Product X8 = new Product("X8", 799); lenovoPC.getProductList().add(X8);
        Product X9 = new Product("X9", 899); lenovoPC.getProductList().add(X9);
        Product X10 = new Product("X10", 999); lenovoPC.getProductList().add(X10);
        Lenovo.setProductCatalog(lenovoPC);
        for(Product p: lenovoPC.getProductList()) p.setBrand("Lenovo");
               
        Supplier Dell = new Supplier();
        sd.getSupplierDirectory().add(Dell);
        ProductCatalog dellPC = new ProductCatalog();
        Product Inspiron1 = new Product("Inspiron1",88); dellPC.getProductList().add(Inspiron1);
        Product Inspiron2 = new Product("Inspiron2",188); dellPC.getProductList().add(Inspiron2);
        Product Inspiron3 = new Product("Inspiron3",288); dellPC.getProductList().add(Inspiron3);
        Product Inspiron4 = new Product("Inspiron4",388); dellPC.getProductList().add(Inspiron4);
        Product Inspiron5 = new Product("Inspiron5",488); dellPC.getProductList().add(Inspiron5);
        Product Inspiron6 = new Product("Inspiron6",588); dellPC.getProductList().add(Inspiron6);
        Product Inspiron7 = new Product("Inspiron7",688); dellPC.getProductList().add(Inspiron7);
        Product Inspiron8 = new Product("Inspiron8",788); dellPC.getProductList().add(Inspiron8);
        Product Inspiron9 = new Product("Inspiron9",888); dellPC.getProductList().add(Inspiron9);
        Product Inspiron10 = new Product("Inspiron10",988); dellPC.getProductList().add(Inspiron10);
        Dell.setProductCatalog(dellPC);
        for(Product p: dellPC.getProductList()) p.setBrand("Dell");
        
        Supplier HP = new Supplier();
        sd.getSupplierDirectory().add(HP);
        ProductCatalog hpPC = new ProductCatalog();
        Product Pavilion1 = new Product("Pavilion1",177); hpPC.getProductList().add(Pavilion1);
        Product Pavilion2 = new Product("Pavilion2",277); hpPC.getProductList().add(Pavilion2);
        Product Pavilion3 = new Product("Pavilion3",377); hpPC.getProductList().add(Pavilion3);
        Product Pavilion4 = new Product("Pavilion4",477); hpPC.getProductList().add(Pavilion4);
        Product Pavilion5 = new Product("Pavilion5",577); hpPC.getProductList().add(Pavilion5);
        Product Pavilion6 = new Product("Pavilion6",677); hpPC.getProductList().add(Pavilion6);
        Product Pavilion7 = new Product("Pavilion7",777); hpPC.getProductList().add(Pavilion7);
        Product Pavilion8 = new Product("Pavilion8",877); hpPC.getProductList().add(Pavilion8);
        Product Pavilion9 = new Product("Pavilion9",977); hpPC.getProductList().add(Pavilion9);
        Product Pavilion10 = new Product("Pavilion10",1077); hpPC.getProductList().add(Pavilion10);
        HP.setProductCatalog(hpPC);
        for(Product p: hpPC.getProductList()) p.setBrand("HP");
        
        Supplier Apple = new Supplier();
        sd.getSupplierDirectory().add(Apple);
        ProductCatalog Mac = new ProductCatalog();
        Product MacBookPro1 = new Product("MacBookPro1",1100); Mac.getProductList().add(MacBookPro1);
        Product MacBookPro2 = new Product("MacBookPro2",1200); Mac.getProductList().add(MacBookPro2);
        Product MacBookPro3 = new Product("MacBookPro3",1300); Mac.getProductList().add(MacBookPro3);
        Product MacBookPro4 = new Product("MacBookPro4",1400); Mac.getProductList().add(MacBookPro4);
        Product MacBookPro5 = new Product("MacBookPro5",1500); Mac.getProductList().add(MacBookPro5);
        Product MacBookPro6 = new Product("MacBookPro6",1600); Mac.getProductList().add(MacBookPro6);
        Product MacBookPro7 = new Product("MacBookPro7",1700); Mac.getProductList().add(MacBookPro7);
        Product MacBookPro8 = new Product("MacBookPro8",1800); Mac.getProductList().add(MacBookPro8);
        Product MacBookPro9 = new Product("MacBookPro9",1900); Mac.getProductList().add(MacBookPro9);
        Product MacBookPro10 = new Product("MacBookPro10",2000); Mac.getProductList().add(MacBookPro10);
        Apple.setProductCatalog(Mac);
        for(Product p: Mac.getProductList()) p.setBrand("Apple");
        
        Supplier Toshiba = new Supplier();
        sd.getSupplierDirectory().add(Toshiba);
        ProductCatalog toshiPC = new ProductCatalog();
        Product Satellite1 = new Product("Satellite1", 66); toshiPC.getProductList().add(Satellite1);
        Product Satellite2 = new Product("Satellite2", 166); toshiPC.getProductList().add(Satellite2);
        Product Satellite3 = new Product("Satellite3", 266); toshiPC.getProductList().add(Satellite3);
        Product Satellite4 = new Product("Satellite4", 366); toshiPC.getProductList().add(Satellite4);
        Product Satellite5 = new Product("Satellite5", 466); toshiPC.getProductList().add(Satellite5);
        Product Satellite6 = new Product("Satellite6", 566); toshiPC.getProductList().add(Satellite6);
        Product Satellite7 = new Product("Satellite7", 666); toshiPC.getProductList().add(Satellite7);
        Product Satellite8 = new Product("Satellite8", 766); toshiPC.getProductList().add(Satellite8);
        Product Satellite9 = new Product("Satellite9", 866); toshiPC.getProductList().add(Satellite9);
        Product Satellite10 = new Product("Satellite10", 966); toshiPC.getProductList().add(Satellite10);
        Toshiba.setProductCatalog(toshiPC);
        for(Product p: toshiPC.getProductList()) p.setBrand("Toshiba");
        
        
        
        for(Supplier s: sd.getSupplierDirectory()){
            for(Product p: s.getProductCatalog().getProductList()){
                System.out.println(p);
            }
        }    
    }
}
