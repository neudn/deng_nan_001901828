/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Solution {
    public static void main(String[] args){
        SupplierDirectory sd = new SupplierDirectory();
        
        Supplier Lenovo = new Supplier();
        sd.getSupplierDirectory().add(Lenovo);
        ProductCatalog lenovoPC = new ProductCatalog();
        Product X1 = new Product(); lenovoPC.getProductList().add(X1);
        Product X2 = new Product(); lenovoPC.getProductList().add(X2);
        Product X3 = new Product(); lenovoPC.getProductList().add(X3);
        Product X4 = new Product(); lenovoPC.getProductList().add(X4);
        Product X5 = new Product(); lenovoPC.getProductList().add(X5);
        Product X6 = new Product(); lenovoPC.getProductList().add(X6);
        Product X7 = new Product(); lenovoPC.getProductList().add(X7);
        Product X8 = new Product(); lenovoPC.getProductList().add(X8);
        Product X9 = new Product(); lenovoPC.getProductList().add(X9);
        Product X10 = new Product(); lenovoPC.getProductList().add(X10);
        Lenovo.setProductCatalog(lenovoPC);
               
        Supplier Dell = new Supplier();
        sd.getSupplierDirectory().add(Dell);
        ProductCatalog dellPC = new ProductCatalog();
        Product Inspiron1 = new Product(); dellPC.getProductList().add(Inspiron1);
        Product Inspiron2 = new Product(); dellPC.getProductList().add(Inspiron2);
        Product Inspiron3 = new Product(); dellPC.getProductList().add(Inspiron3);
        Product Inspiron4 = new Product(); dellPC.getProductList().add(Inspiron4);
        Product Inspiron5 = new Product(); dellPC.getProductList().add(Inspiron5);
        Product Inspiron6 = new Product(); dellPC.getProductList().add(Inspiron6);
        Product Inspiron7 = new Product(); dellPC.getProductList().add(Inspiron7);
        Product Inspiron8 = new Product(); dellPC.getProductList().add(Inspiron8);
        Product Inspiron9 = new Product(); dellPC.getProductList().add(Inspiron9);
        Product Inspiron10 = new Product(); dellPC.getProductList().add(Inspiron10);
        Dell.setProductCatalog(dellPC);
        
        Supplier HP = new Supplier();
        sd.getSupplierDirectory().add(HP);
        ProductCatalog hpPC = new ProductCatalog();
        Product Pavilion1 = new Product(); hpPC.getProductList().add(Pavilion1);
        Product Pavilion2 = new Product(); hpPC.getProductList().add(Pavilion2);
        Product Pavilion3 = new Product(); hpPC.getProductList().add(Pavilion3);
        Product Pavilion4 = new Product(); hpPC.getProductList().add(Pavilion4);
        Product Pavilion5 = new Product(); hpPC.getProductList().add(Pavilion5);
        Product Pavilion6 = new Product(); hpPC.getProductList().add(Pavilion6);
        Product Pavilion7 = new Product(); hpPC.getProductList().add(Pavilion7);
        Product Pavilion8 = new Product(); hpPC.getProductList().add(Pavilion8);
        Product Pavilion9 = new Product(); hpPC.getProductList().add(Pavilion9);
        Product Pavilion10 = new Product(); hpPC.getProductList().add(Pavilion10);
        HP.setProductCatalog(hpPC);
        
        Supplier Apple = new Supplier();
        sd.getSupplierDirectory().add(Apple);
        ProductCatalog Mac = new ProductCatalog();
        Product MacBookPro1 = new Product(); Mac.getProductList().add(MacBookPro1);
        Product MacBookPro2 = new Product(); Mac.getProductList().add(MacBookPro2);
        Product MacBookPro3 = new Product(); Mac.getProductList().add(MacBookPro3);
        Product MacBookPro4 = new Product(); Mac.getProductList().add(MacBookPro4);
        Product MacBookPro5 = new Product(); Mac.getProductList().add(MacBookPro5);
        Product MacBookPro6 = new Product(); Mac.getProductList().add(MacBookPro6);
        Product MacBookPro7 = new Product(); Mac.getProductList().add(MacBookPro7);
        Product MacBookPro8 = new Product(); Mac.getProductList().add(MacBookPro8);
        Product MacBookPro9 = new Product(); Mac.getProductList().add(MacBookPro9);
        Product MacBookPro10 = new Product(); Mac.getProductList().add(MacBookPro10);
        Apple.setProductCatalog(Mac);
        
        Supplier Toshiba = new Supplier();
        sd.getSupplierDirectory().add(Toshiba);
        ProductCatalog toshiPC = new ProductCatalog();
        Product Satellite1 = new Product(); toshiPC.getProductList().add(Satellite1);
        Product Satellite2 = new Product(); toshiPC.getProductList().add(Satellite2);
        Product Satellite3 = new Product(); toshiPC.getProductList().add(Satellite3);
        Product Satellite4 = new Product(); toshiPC.getProductList().add(Satellite4);
        Product Satellite5 = new Product(); toshiPC.getProductList().add(Satellite5);
        Product Satellite6 = new Product(); toshiPC.getProductList().add(Satellite6);
        Product Satellite7 = new Product(); toshiPC.getProductList().add(Satellite7);
        Product Satellite8 = new Product(); toshiPC.getProductList().add(Satellite8);
        Product Satellite9 = new Product(); toshiPC.getProductList().add(Satellite9);
        Product Satellite10 = new Product(); toshiPC.getProductList().add(Satellite10);
        Toshiba.setProductCatalog(toshiPC);
        
        for(Supplier s: sd.getSupplierDirectory()){
            for(Product p: s.getProductCatalog().getProductList()){
                if((p.getModelNumber()-1)/10 == 0) System.out.println("This is a Lenovo computer");
                if((p.getModelNumber()-1)/10 == 1) System.out.println("This is a Dell computer");
                if((p.getModelNumber()-1)/10 == 2) System.out.println("This is a HP computer");
                if((p.getModelNumber()-1)/10 == 3) System.out.println("This is a Apple computer");
                if((p.getModelNumber()-1)/10 == 4) System.out.println("This is a Toshiba computer");
            }
        }    
    }
}
