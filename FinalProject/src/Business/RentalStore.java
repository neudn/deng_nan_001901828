/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

public class RentalStore {
    String Location;
    CarList carList;
    PersonList personList;

    public String getLocation() {
        return Location;
    }

    public void setLocation(String Location) {
        this.Location = Location;
    }
    
    public void addCar(Car car){
        carList.addCar(car);
    }
    
    public void deleteCar(Car car){
        carList.deleteCar(car);
    }
    
    public void addPerson(Person person){
        personList.addPerson(person);
    }
    
    public void deletePerson(Person person){
        personList.deletePerson(person);
    }
}
