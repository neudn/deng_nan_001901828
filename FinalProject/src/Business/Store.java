/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Store {
    
    CarList carList;
    PersonList employeeList;
    String name;
    Car car1, car2, car3;
    OrderList orderList;
    
    public Store(){
        carList = new CarList();
        employeeList = new PersonList();
        car1 = carList.createCar();
        car1.setName("Honda Civic");
        car1.setType("Economic");
        car2 = carList.createCar();
        car2.setName("Ford Escape");
        car2.setType("SUV");
        car3 = carList.createCar();
        car2.setName("Tesla 3");
        car2.setType("Luxury");
        orderList = new OrderList();
    }

    public CarList getCarList() {
        return carList;
    }

    public void setCarList(CarList carList) {
        this.carList = carList;
    }

    public PersonList getEmployeeList() {
        return employeeList;
    }

    public void setEmployeeList(PersonList employeeList) {
        this.employeeList = employeeList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public OrderList getOrderList() {
        return orderList;
    }

    public void setOrderList(OrderList orderList) {
        this.orderList = orderList;
    }  
    
    @Override
    public String toString(){
        return this.getName();
    }
}
