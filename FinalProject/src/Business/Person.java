/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

public class Person {
    private String firstName;
    private String lastName;
    private int age;
    private String homeAddress;
    private String BankAccount;
    private DriverLicencre driverLincence;
    private String organization;

    public Person() {
        
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String name) {
        this.firstName = name;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String name){
        this.lastName = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getBankAccount() {
        return BankAccount;
    }

    public void setBankAccount(String BankAccount) {
        this.BankAccount = BankAccount;
    }

    public DriverLicencre getDriverLincence() {
        return driverLincence;
    }

    public void setDriverLincence(DriverLicencre driverLincence) {
        this.driverLincence = driverLincence;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
    
    @Override
    public String toString(){
        return this.getFirstName() + this.getLastName();
    }
}
