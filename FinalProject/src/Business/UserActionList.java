/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class UserActionList {
    private ArrayList<UserAction> al;
    
    public UserActionList(){
        al = new ArrayList<>();
    }

    public ArrayList<UserAction> getAl() {
        return al;
    }

    public void setAl(ArrayList<UserAction> al) {
        this.al = al;
    }
    
    public UserAction addUserAction(){
        UserAction ua = new UserAction();
        al.add(ua);
        return ua;
    }
    
    public void addUserAction(String s){
        UserAction ua = new UserAction();
        ua.setActionName(s);
        al.add(ua);
    }
}
