/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class LogSessionHistory {
    
    private LogSession lastSession;
    private ArrayList<LogSession> al;
    
    public LogSessionHistory(){
        al = new ArrayList<>();
    }

    public ArrayList<LogSession> getAl() {
        return al;
    }

    public void setAl(ArrayList<LogSession> al) {
        this.al = al;
    }
    
    public LogSession addLogSession(){
        LogSession logSession = new LogSession();
        al.add(logSession);
        return logSession;
    }
    
    public void addLogSession(LogSession ls){
        al.add(ls);
    }
    
    public int locateLastSession(UserAccount ua){
        int res = -1;
        for(int i = 0; i < al.size(); i++){
            if(ua.equals(al.get(i).getUserAccount())) res = i;
        }
        return res;
    }
}
