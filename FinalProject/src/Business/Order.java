/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author dn
 */
public class Order {
    private Car car;
    private int orderNumber;
    private static int count = 0;
    private int commision;
    private int cost;
    private int temPrice;
    private String status;
    private String customerName;
    private String salespersonName;
    private Date startTime;
    private Date endTime;
    
    public Order(){
        count++;
        orderNumber = count;
        cost = 0;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getPrice() {
        return temPrice;
    }

    public void setPrice(int price) {
        this.temPrice = price;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    } 

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getTemPrice() {
        return temPrice;
    }

    public void setTemPrice(int temPrice) {
        this.temPrice = temPrice;
    }

    public String getSalespersonName() {
        return salespersonName;
    }

    public void setSalespersonName(String salespersonName) {
        this.salespersonName = salespersonName;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    
    
    
    @Override
    public String toString(){
        return String.valueOf(this.customerName);
    }
}
