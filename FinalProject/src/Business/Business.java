/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Business {

    private static Business business = null;
    private UserAccountDirectory userAccountDirectory;
    private PersonList personList;
    private CarList carList;
    private StoreList storeList;
    private OrderList orderList;
    private LogSessionHistory logSessionHistory;
    private LogSession logSession;
    
    private Business(){
        userAccountDirectory = new UserAccountDirectory();
        personList = new PersonList();
        carList = new CarList();
        storeList = new StoreList();
        orderList = new OrderList();
        logSessionHistory = new LogSessionHistory();
    }
    
    public static Business getInstance(){
        if(business != null){
            return business;
        }
        return new Business();
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public PersonList getPersonList() {
        return personList;
    }

    public void setPersonList(PersonList personList) {
        this.personList = personList;
    }

    public CarList getCarList() {
        return carList;
    }

    public void setCarList(CarList carList) {
        this.carList = carList;
    }

    public StoreList getStoreList() {
        return storeList;
    }

    public void setStoreList(StoreList storeList) {
        this.storeList = storeList;
    }

    public OrderList getOrderList() {
        return orderList;
    }

    public void setOrderList(OrderList orderList) {
        this.orderList = orderList;
    }    

    public LogSessionHistory getLogSessionHistory() {
        return logSessionHistory;
    }

    public void setLogSessionHistory(LogSessionHistory logSessionHistory) {
        this.logSessionHistory = logSessionHistory;
    }

    public LogSession getLogSession() {
        return logSession;
    }

    public void setLogSession(LogSession logSession) {
        this.logSession = logSession;
    }
 
    
}
