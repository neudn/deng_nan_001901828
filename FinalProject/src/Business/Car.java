/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Arrays;
import java.util.Random;

public class Car {
    private String name;
    private String plate;
    private String type;
    private int rate;
    private int collision;
    private int liability;
    private boolean available;
    private int upBound;
    private int lowerBound;
    private String accidents;
    
    public Car(){
        this.rate = 50;
        this.collision = 20;
        this.liability = 10;
        this.available = true;
        accidents = "No";
        this.plate = this.generatePlate();
    }    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getCollision() {
        return collision;
    }

    public void setCollision(int collision) {
        this.collision = collision;
    }

    public int getLiability() {
        return liability;
    }

    public void setLiability(int liability) {
        this.liability = liability;
    }

    public boolean isAvailability() {
        return available;
    }

    public void setAvailability(boolean availability) {
        this.available = availability;
    }

    public int getUpBound() {
        return upBound;
    }

    public void setUpBound(int upBound) {
        this.upBound = upBound;
    }

    public int getLowerBound() {
        return lowerBound;
    }

    public void setLowerBound(int lowerBound) {
        this.lowerBound = lowerBound;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public String getAccidents() {
        return accidents;
    }

    public void setAccidents(String accidents) {
        this.accidents = accidents;
    }
    
    public String generatePlate(){
        char[] plate = new char[6];
        Random ran = new Random();
        for(int i = 0; i < 6; i++){
            plate[i] = (char)(ran.nextInt(25)+97);
        }
        return Arrays.toString(plate);
    }
    
    @Override
    public String toString(){
        return this.name;
    }
}
