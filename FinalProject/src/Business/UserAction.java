/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import org.joda.time.DateTime;

/**
 *
 * @author dn
 */
public class UserAction {
    String actionName;
    String safetyLevel;
    DateTime dt;
    
    public UserAction(){
        dt = new DateTime();     
    }
    
    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getSafetyLevel() {
        return safetyLevel;
    }

    public void setSafetyLevel(String safetyLevel) {
        this.safetyLevel = safetyLevel;
    } 

    public DateTime getDt() {
        return dt;
    }

    public void setDt(DateTime dt) {
        this.dt = dt;
    }
    
    
    
    @Override
    public String toString(){
        return this.getDt().toString();
    }
}
