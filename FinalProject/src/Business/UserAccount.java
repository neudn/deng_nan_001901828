/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class UserAccount {
    
    public static final String ADMIN_ROLE = "admin";
    public static final String EMPLOYEE_ROLE = "employee";
    public static final String CUSTOMER_ROLE = "customer";
    public static final String SECURITY_ROLE = "security";
    public static final String MANAGER_ROLE = "manager";
    
    private Person person;
    private String userName;
    private String password;
    private String role;
    private Order currentOrder;
    
    public UserAccount(){
        person = new Person();
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Order getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order order) {
        this.currentOrder = order;
    }
}
