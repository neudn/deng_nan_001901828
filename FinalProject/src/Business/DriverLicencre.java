/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.Date;

/**
 *
 * @author dn
 */
class DriverLicencre {
    private Date date;
    private String lincenseNumber;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getLincenseNumber() {
        return lincenseNumber;
    }

    public void setLincenseNumber(String lincenseNumber) {
        this.lincenseNumber = lincenseNumber;
    }
    
    
}
