/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class PersonList {
    ArrayList<Person> al;
    
    public PersonList(){
        al = new ArrayList<>();
    }

    public ArrayList<Person> getAl() {
        return al;
    }

    public void setAl(ArrayList<Person> al) {
        this.al = al;
    }
    
    public Person createPerson(){
        Person person = new Person();
        al.add(person);
        return person;
    }
    
    public void addPerson(Person person){
        al.add(person);
    }
    
    public void deletePerson(Person p){
        al.remove(p);
    }
}
