 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

/**
 *
 * @author Bala
 */
public class ConfigureBusiness {
    public static Business initializeBusiness(){
        Business business = Business.getInstance();
        UserAccount userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.getPerson().setFirstName("Admin");
        userAccount.getPerson().setLastName("Admin");
        userAccount.getPerson().setOrganization("NEU");
        userAccount.setUserName("admin");
        userAccount.setPassword("admin");
        userAccount.setRole(UserAccount.ADMIN_ROLE);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.getPerson().setFirstName("Nan");
        userAccount.getPerson().setLastName("Deng");
        userAccount.getPerson().setOrganization("Vber");
        userAccount.setUserName("dn");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.EMPLOYEE_ROLE);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.getPerson().setFirstName("Kal");
        userAccount.getPerson().setLastName("Bug");
        userAccount.getPerson().setOrganization("Aoston");
        userAccount.setUserName("kb");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.CUSTOMER_ROLE);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.getPerson().setFirstName("Jack");
        userAccount.getPerson().setLastName("Chen");
        userAccount.getPerson().setOrganization("Aoston");
        userAccount.setUserName("jc");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.SECURITY_ROLE);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.getPerson().setFirstName("Joe");
        userAccount.getPerson().setLastName("Aoun");
        userAccount.getPerson().setOrganization("Aoston");
        userAccount.setUserName("ja");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.MANAGER_ROLE);
                
        Car car = business.getCarList().createCar();
        car.setName("Tesla");
        car.setPlate("SSEEXX");
        car.setLowerBound(100);
        car.setUpBound(200);
        
        StoreList storeList = business.getStoreList();
        Store store1 = storeList.addStore();
        store1.setName("Cambridge");
        Store store2 = storeList.addStore();
        store2.setName("Brookline");
        Store store3 = storeList.addStore();
        store3.setName("Malden");
        
        return business;
    }
}
