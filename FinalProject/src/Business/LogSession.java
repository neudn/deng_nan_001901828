/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import org.joda.time.DateTime;

/**
 *
 * @author dn
 */
public class LogSession {
    private DateTime date;
    private UserAccount userAccount;
    private UserActionList userActionList;
    private String securityLevel;
    private String location;
    private int number;
    private String dataString;

    public LogSession() {
        this.userActionList = new UserActionList();
        date = new DateTime();
        dataString = date.toString();
        number = 0;
    }

    public DateTime getDate() {
        return date;
    }
   
    public void setDate(DateTime date) {
        this.date = date;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserActionList getUserActionList() {
        return userActionList;
    }

    public void setUserActionList(UserActionList userActionList) {
        this.userActionList = userActionList;
    }

    public String getSecurityLevel() {
        return securityLevel;
    }

    public void setSecurityLevel(String securityLevel) {
        this.securityLevel = securityLevel;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
    
    @Override
    public String toString(){
        return this.dataString;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
    
    public void incrementNum(){
        this.number++;
    }
}
