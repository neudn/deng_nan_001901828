/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

public class CarList {
    ArrayList<Car> al;

    public CarList(){
        al = new ArrayList<>();
    }
    
    public ArrayList<Car> getAl() {
        return al;
    }

    public void setAl(ArrayList<Car> al) {
        this.al = al;
    }
    
    public Car createCar(){
        Car car = new Car();
        this.al.add(car);
        return car;
    }
    
    public void addCar(Car car){
        this.al.add(car);
    }
    
    public void deleteCar(Car car){
        al.remove(car);
    }
}
