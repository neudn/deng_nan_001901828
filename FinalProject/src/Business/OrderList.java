/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class OrderList {
    ArrayList<Order> orderList;
    
    public OrderList(){
        orderList = new ArrayList<>();
    }    

    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
    
    public Order CreateOrder(){
        Order order = new Order();
        orderList.add(order);
        return order;
    }
    
    public void addOrder(Order order){
        orderList.add(order);
    }
}
