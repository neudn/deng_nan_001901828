package UI.StoreAdminRole;

import Business.*;
import UI.StarMarketAdminRole.ViewProduct;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Rushabh
 */
public class ManageProductInventoryJPanel extends javax.swing.JPanel {

    /** Creates new form ManageProductInventoryJPanel */
    private JPanel userProcessContainer;
    private ProductCatalog productCatalog;
    private Store store;
    
    public ManageProductInventoryJPanel(JPanel upc, ProductCatalog pc, Store s) {
        initComponents();
        userProcessContainer = upc;
        productCatalog = pc;
        store = s;
        txtStoreName.setText(s.getLocation());
        refreshCatalogTable();
        refreshInventoryTable();
    }

    public void refreshCatalogTable() {
        DefaultTableModel model = (DefaultTableModel)productCatalogTable.getModel();
        model.setRowCount(0);
        for(Product p : productCatalog.getProductCatalog()) {
            Object row[] = new Object[3];
            row[0] = p;
            row[1] = p.getAvilability();
            row[2] = p.getSuggestedPrice();
            model.addRow(row);
        }
    }
    
     public void refreshInventoryTable() {
        DefaultTableModel model = (DefaultTableModel)productInventoryTable.getModel();
        model.setRowCount(0);      
        for(Product p : store.getInventory().getInventory()) {
            Object row[] = new Object[3];
            row[0] = p;
            row[1] = p.getAvilability();
            row[2] = p.getSuggestedPrice();
            model.addRow(row);
        }
    }
    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        productInventoryTable = new javax.swing.JTable();
        btnView = new javax.swing.JButton();
        btnPurchase = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtStoreName = new javax.swing.JTextField();
        btnRecall = new javax.swing.JButton();
        btnSell = new javax.swing.JButton();
        txtPurchase = new javax.swing.JTextField();
        txtRecall = new javax.swing.JTextField();
        txtSell = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        productCatalogTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txtSearch = new javax.swing.JTextField();
        BtnSearch = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("Manage Product Inventory");

        productInventoryTable.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        productInventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Product Name", "Availability", "Price"
            }
        ));
        jScrollPane1.setViewportView(productInventoryTable);

        btnView.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnView.setText("View Product Detail >>");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnPurchase.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPurchase.setText("Purchase Product ");
        btnPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPurchaseActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Store:");

        txtStoreName.setEditable(false);
        txtStoreName.setFont(new java.awt.Font("Georgia", 1, 18)); // NOI18N
        txtStoreName.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtStoreName.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED, null, new java.awt.Color(102, 102, 102), null, null));
        txtStoreName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtStoreNameActionPerformed(evt);
            }
        });

        btnRecall.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnRecall.setText("Recall Product ");
        btnRecall.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRecallActionPerformed(evt);
            }
        });

        btnSell.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSell.setText("Sell Product ");
        btnSell.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSellActionPerformed(evt);
            }
        });

        productCatalogTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        productCatalogTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Quantity", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(productCatalogTable);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Market Product Catalog");

        BtnSearch.setText("Search");
        BtnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(BtnSearch))
                            .addComponent(jLabel1)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(43, 43, 43)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtStoreName, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 500, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createSequentialGroup()
                                    .addGap(9, 9, 9)
                                    .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(50, 50, 50)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtRecall)
                                        .addComponent(txtPurchase)
                                        .addComponent(txtSell))
                                    .addGap(52, 52, 52)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnRecall, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btnSell, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addContainerGap(44, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(jLabel1)
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(BtnSearch))
                .addGap(23, 23, 23)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(txtStoreName, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addComponent(btnView)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPurchase)
                    .addComponent(txtPurchase, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnRecall)
                    .addComponent(btnBack)
                    .addComponent(txtRecall, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSell)
                    .addComponent(txtSell, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents
    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed

        int row = productCatalogTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Product p = (Product)productCatalogTable.getValueAt(row, 0);
        ViewProductDetailJPanel vpdjp = new ViewProductDetailJPanel(userProcessContainer, p);
        userProcessContainer.add("ViewProductDetailJPanel", vpdjp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnViewActionPerformed

    private void btnPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPurchaseActionPerformed
        int numToPurchase = Integer.parseInt(txtPurchase.getText());
        int row = productCatalogTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Product p = (Product)productCatalogTable.getValueAt(row, 0);
        for(Product pp: productCatalog.getProductCatalog()){
            if(pp.equals(p)){
                int numInStock = pp.getAvilability();
                if(numInStock == 0){
                    JOptionPane.showMessageDialog(null, "Out of Stock!", "Warning", JOptionPane.WARNING_MESSAGE);
                } else if(numInStock < numToPurchase) {
                    JOptionPane.showMessageDialog(null, "We need more products!", "Warning", JOptionPane.WARNING_MESSAGE);
                } else {
                    pp.setAvilability(pp.getAvilability() - numToPurchase);
                    for(Product ppp: store.getInventory().getInventory()){
                        if(ppp.getName().equals(p.getName())){
                            ppp.setAvilability(ppp.getAvilability() + numToPurchase);
                            break;
                        }
                    }
                }
            }
        }
        refreshInventoryTable();
        refreshCatalogTable();
    }//GEN-LAST:event_btnPurchaseActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRecallActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRecallActionPerformed
        // TODO add your handling code here:
        int numToRecall = Integer.parseInt(txtRecall.getText());
        int row = productCatalogTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Product p = (Product)productCatalogTable.getValueAt(row, 0);
        for(Product pp: productCatalog.getProductCatalog()){
            if(pp.getName() == p.getName()){
                int numInStock = p.getAvilability();
                if(numInStock < numToRecall) {
                    JOptionPane.showMessageDialog(null, "We need more products!", "Warning", JOptionPane.WARNING_MESSAGE);
                } else {
                    pp.setAvilability(pp.getAvilability() + numToRecall);
                    p.setAvilability(p.getAvilability() - numToRecall);
                }
                break;
            }
        }
        refreshInventoryTable();
        refreshCatalogTable();
    }//GEN-LAST:event_btnRecallActionPerformed

    private void btnSellActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSellActionPerformed
        // TODO add your handling code here:
        int numToSell = Integer.parseInt(txtSell.getText());
        
        int row = productCatalogTable.getSelectedRow();
        if(row<0){
            JOptionPane.showMessageDialog(null, "Pls select a row!!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
        Product p = (Product)productCatalogTable.getValueAt(row, 0);
        for(Product pp: store.getInventory().getInventory()){
            if(pp.getName().equals(p.getName())){
                int numInStore = pp.getAvilability();
                if(numInStore < numToSell) {
                    JOptionPane.showMessageDialog(null, "We need more products!", "Warning", JOptionPane.WARNING_MESSAGE);
                } else {
                    pp.setAvilability(pp.getAvilability() - numToSell);
                }
                break;
            }
        }
    }//GEN-LAST:event_btnSellActionPerformed

    private void BtnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BtnSearchActionPerformed
        // TODO add your handling code here:
        String searchKeyWord = txtSearch.getText();
        if(productCatalog.searchProduct(searchKeyWord) != null){
            Product product = productCatalog.searchProduct(searchKeyWord);
            ViewProductDetailJPanel vp = new ViewProductDetailJPanel(userProcessContainer, product);
            userProcessContainer.add("View Product", vp);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.next(userProcessContainer);
        } else {
            JOptionPane.showMessageDialog(null, "No such product!", "Warning", JOptionPane.WARNING_MESSAGE);
        }
    }//GEN-LAST:event_BtnSearchActionPerformed

    private void txtStoreNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtStoreNameActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtStoreNameActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton BtnSearch;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnPurchase;
    private javax.swing.JButton btnRecall;
    private javax.swing.JButton btnSell;
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable productCatalogTable;
    private javax.swing.JTable productInventoryTable;
    private javax.swing.JTextField txtPurchase;
    private javax.swing.JTextField txtRecall;
    private javax.swing.JTextField txtSearch;
    private javax.swing.JTextField txtSell;
    private javax.swing.JTextField txtStoreName;
    // End of variables declaration//GEN-END:variables
}
