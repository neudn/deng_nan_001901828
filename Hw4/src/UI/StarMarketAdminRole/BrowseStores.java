/*
 * BrowseStores.java
 *
 * Created on October 10, 2008, 9:10 AM
 */
package UI.StarMarketAdminRole;

import Business.*;
import UI.StoreAdminRole.*;
import java.awt.CardLayout;
import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.data.category.DefaultCategoryDataset;


/**
 *
 * @author Rushabh
 */
public class BrowseStores extends javax.swing.JPanel {

    private StoreList storeList;
    private ProductCatalog productCatalog;
    private JPanel userProcessContainer;
    
    /** Creates new form BrowseProducts */
    public BrowseStores(JPanel upc, ProductCatalog pc, StoreList sl) {
        initComponents();
        this.userProcessContainer = upc;
        this.storeList = sl;
        this.productCatalog = pc;
        populateStoreCombo();
        refreshCatalogTable();
        populateInventoryTable();
    }
    
    private void refreshCatalogTable(){
        DefaultTableModel dtm =  (DefaultTableModel)catalogTable.getModel();
        dtm.setRowCount(0);
        for(Product product: productCatalog.getProductCatalog()){
            Object row[] = new Object[3];
            row[0] = product;
            row[1] = product.getAvilability();
            row[2] = product.getSuggestedPrice();
            dtm.addRow(row);
        }    
    }

    private void populateStoreCombo(){
        suppComboBox1.removeAllItems();
        
        for(Store store: storeList.getStoreList()){
            suppComboBox1.addItem(store);
        }
    }
    
    private void populateInventoryTable(){
        DefaultTableModel dtm =  (DefaultTableModel)inventoryTable.getModel();
        Store store = (Store)suppComboBox1.getSelectedItem();
        dtm.setRowCount(0);
        if(store != null){
            for(Product product: store.getInventory().getInventory()){
                Object row[] = new Object[3];
                row[0] = product;
                row[1] = product.getAvilability();
                row[2] = product.getSuggestedPrice();
                dtm.addRow(row);
            }    
        }
    }
    
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        catalogTable = new javax.swing.JTable();
        suppComboBox1 = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        viewProdjButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        inventoryTable = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        ViewProfit = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(750, 511));

        catalogTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        catalogTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Quantity", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(catalogTable);

        suppComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                suppComboBox1ActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Store");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Store Product Inventory");

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnBack.setText("<< Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        viewProdjButton2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        viewProdjButton2.setText("View Product Detail");
        viewProdjButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewProdjButton2ActionPerformed(evt);
            }
        });

        inventoryTable.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        inventoryTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Name", "Quantity", "Price"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(inventoryTable);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Market Product Catalog");

        ViewProfit.setText("View Most Profitable");
        ViewProfit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewProfitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(61, 61, 61)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(viewProdjButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ViewProfit)
                                .addGap(65, 65, 65))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(89, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(71, 71, 71)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 590, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(89, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(67, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(suppComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(jLabel2)
                .addGap(126, 126, 126)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(viewProdjButton2)
                    .addComponent(ViewProfit))
                .addGap(27, 27, 27)
                .addComponent(btnBack)
                .addGap(24, 24, 24))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                    .addContainerGap(264, Short.MAX_VALUE)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(106, 106, 106)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void suppComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_suppComboBox1ActionPerformed
        // TODO add your handling code here:
        populateInventoryTable();        
    }//GEN-LAST:event_suppComboBox1ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void viewProdjButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewProdjButton2ActionPerformed
        // TODO add your handling code here:
        int selectedRow = catalogTable.getSelectedRow();
        if(selectedRow < 0){
            JOptionPane.showMessageDialog(this, "Select a row", "Warning", JOptionPane.WARNING_MESSAGE);
            return;
        }
        Product p = (Product) catalogTable.getValueAt(selectedRow, 0);
        ViewProductDetailJPanel viewPanel = new ViewProductDetailJPanel(userProcessContainer, p);
        userProcessContainer.add("ViewProduct", viewPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_viewProdjButton2ActionPerformed

    private void ViewProfitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewProfitActionPerformed
        // TODO add your handling code here:
       DefaultCategoryDataset dataset = new DefaultCategoryDataset();
       Store store = (Store)suppComboBox1.getSelectedItem();
       for(int i = 1; i < store.getInventory().getInventory().size(); i++){
            for(int j = store.getInventory().getInventory().size() - 1; j > 0; j--){
                if(store.getInventory().getInventory().get(j).getProfit() > store.getInventory().getInventory().get(j-1).getProfit()){
                    Product first = store.getInventory().getInventory().get(i);
                    Product second = store.getInventory().getInventory().get(i-1);
                    Product temp = first;
                    first = second;
                    second = temp;
                    store.getInventory().getInventory().set(i, first);
                    store.getInventory().getInventory().set(i-1, second);
                }
            }
        }
       if(store.getInventory().getInventory().size() <= 5){
        for(Product product: store.getInventory().getInventory()){
           if(product.getAvilability() > 0){
           dataset.setValue(product.getProfit(), "Profit",product.getName());
           }
        }
       } else {
        int num = 0;
        for(Product product: store.getInventory().getInventory()){
           if(product.getAvilability() > 0){
            dataset.setValue(product.getProfit(), "Profit",product.getName());
            num++;
            if(num == 5) break;
           }
        }   
       }

       JFreeChart chart = ChartFactory.createBarChart("Rank of Profit", "","Profit", dataset);
       CategoryPlot p = chart.getCategoryPlot();
       p.setRangeGridlinePaint(Color.BLACK);
       ChartFrame frame = new ChartFrame("Chart", chart);
       frame.setVisible(true);
       frame.setSize(400,400);
    }//GEN-LAST:event_ViewProfitActionPerformed

    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton ViewProfit;
    private javax.swing.JButton btnBack;
    private javax.swing.JTable catalogTable;
    private javax.swing.JTable inventoryTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox suppComboBox1;
    private javax.swing.JButton viewProdjButton2;
    // End of variables declaration//GEN-END:variables
}
