/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class ProductInventory {
    private ArrayList<Product> inventory;
    
    public ProductInventory(){
        inventory = new ArrayList<Product>();
    }

    public ArrayList<Product> getInventory() {
        return inventory;
    }
    
    public Product addProduct(){
        Product p = new Product();
        inventory.add(p);
        return p;
    }
    
    public void sort(){
        for(int i = 0; i < inventory.size(); i++){
            for(int j = inventory.size() - 1; j > i; j++){
                if(this.inventory.get(j).getProfit() < inventory.get(j+1).getProfit()){
                    Product first = inventory.get(i);
                    Product second = inventory.get(i+1);
                    Product temp = first;
                    first = second;
                    second = temp;
                    inventory.set(i, first);
                    inventory.set(i+1, second);
                }
            }
        }
    }
}
