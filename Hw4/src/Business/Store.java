/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Store {
    private String location;
    private ProductInventory inventory;
    
    public Store(){
        inventory = new ProductInventory();
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ProductInventory getInventory() {
        return inventory;
    }

    public void setInventory(ProductInventory inventory) {
        this.inventory = inventory;
    }
    
    @Override
    public String toString(){
        return getLocation();
    }
}
