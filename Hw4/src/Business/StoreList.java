/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class StoreList {
    ArrayList<Store> storeList;
    
    public StoreList(){
        storeList = new ArrayList<Store>();
    }

    public ArrayList<Store> getStoreList() {
        return storeList;
    }
    
    public Store addStore(){
        Store store = new Store();
        storeList.add(store);
        return store;
    }
}
