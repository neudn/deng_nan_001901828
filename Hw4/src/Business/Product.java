/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Product{
    
    private String brand;
    private String name;
    private int modelNumber;
    private int suggestedPrice;
    private int purchasePrice;
    private String onShelfTime;
    private int availability;    // quantity
    private String color;
    private String length;
    private String weight;
    private String profit;
    
    private static int count = 0;
    
    public Product() {
        count++;
        modelNumber = count;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSuggestedPrice() {
        return suggestedPrice;
    }

    public void setSuggestedPrice(int suggestedPrice) {
        this.suggestedPrice = suggestedPrice;
    }

    public int getPurchasePrice() {
        return purchasePrice;
    }

    public void setPurchasePrice(int purchasePrice) {
        this.purchasePrice = purchasePrice;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getOnShelfTime() {
        return onShelfTime;
    }

    public void setOnShelfTime(String onShelfTime) {
        this.onShelfTime = onShelfTime;
    }

    public int getAvilability() {
        return availability;
    }

    public void setAvilability(int avilability) {
        this.availability = avilability;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }
    
    public int getProfit(){
        return this.getSuggestedPrice() - this.getPurchasePrice();
    }
    
    @Override
    public String toString(){
        return this.getName();
    }
    
    public int compareTo(Product p){
        return (this.getSuggestedPrice()-this.getPurchasePrice())-(p.getSuggestedPrice()-p.getPurchasePrice());
    }

}
