/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class VitalSign {
    private static int count = 0;
    
    private String ProductName;
    private int modelNumber;
    private int availability;
    private double price;
    
    private String bloodPressure;
    private String heartRate;
    private String weight;
    private String respiratoryRate;
    private String date;

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getHeartRate() {
        return heartRate;
    }

    public void setHeartRate(String heartRate) {
        this.heartRate = heartRate;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getRespiratoryRate() {
        return respiratoryRate;
    }

    public void setRespiratoryRate(String respiratoryRate) {
        this.respiratoryRate = respiratoryRate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    
    public VitalSign(){
        count++;
        modelNumber = count;
        
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public int getModelNumber() {
        return modelNumber;
    }

    public void setModelNumber(int modelNumber) {
        this.modelNumber = modelNumber;
    }

    public int getAvailability() {
        return availability;
    }

    public void setAvailability(int availability) {
        this.availability = availability;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    @Override
    public String toString(){
        return this.getDate();
    }
    
    public String isNormal(VitalSign vs, int age){
        int hr = Integer.parseInt(vs.getHeartRate());
        int rr = Integer.parseInt(vs.getRespiratoryRate());
        int bp = Integer.parseInt(vs.getBloodPressure());
        int w = Integer.parseInt(vs.getWeight());
        
        if(age >= 1 && age <= 3 
         &&hr >= 80 && hr <= 130
         &&rr >= 20 && rr <= 30
         &&bp >= 80 && bp <= 110
         &&w >= 22 && w <= 31    
        ) return "Normal";
        
        if(age >= 4 && age <= 5 
         &&hr >= 80 && hr <= 120
         &&rr >= 20 && rr <= 30
         &&bp >= 80 && bp <= 110
         &&w >= 31 && w <= 40   
        ) return "Normal";
        
        if(age >= 6 && age <= 12 
         &&hr >= 70 && hr <= 110
         &&rr >= 20 && rr <= 30
         &&bp >= 80 && bp <= 120
         &&w >= 41 && w <= 92   
        ) return "Normal";
        
        if(age >= 13 
         &&hr >= 55 && hr <= 105
         &&rr >= 12 && rr <= 20
         &&bp >= 110 && bp <= 120
         &&w >= 110  
        ) return "Normal";
        
        return "Abnormal";
        
    }
    
}
