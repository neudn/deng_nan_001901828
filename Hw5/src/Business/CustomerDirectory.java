/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class CustomerDirectory {
    ArrayList<Person> al;
    
    public CustomerDirectory(){
        al = new ArrayList<>();
    }

    public ArrayList<Person> getAl() {
        return al;
    }
    
    public void addCustomer(Person p){
        al.add(p);
    }
}
