/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class UserAccount extends Person{
    
    public static final String ADMIN_ROLE = "admin";
    public static final String EMPLOYEE_ROLE = "employee";
    public static final String CUSTOMER_ROLE = "customer";
    
    private static int count = 1000;
    private int userId;
    private String password;
    private String userName;
    private Boolean isActive;
    private String role;
    private Order currentOrder;
    
    public UserAccount(){
        userId = count++;
    }

    public int getUserId() {
        return userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
    
    public String toString(){
        return this.getFirstName();
    }

    public Order getCurrentOrder() {
        return currentOrder;
    }

    public void setCurrentOrder(Order currentOrder) {
        this.currentOrder = currentOrder;
    }
    
    
}
