/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class MasterOrderCatalog {
    
    ArrayList<Order> orderCatalog;
    
    public MasterOrderCatalog(){
        orderCatalog = new ArrayList<Order>();
    }
    
    public ArrayList<Order> getOrderCatalog(){
        return this.orderCatalog;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MasterOrderCatalog other = (MasterOrderCatalog) obj;
        if (this.orderCatalog != other.orderCatalog && (this.orderCatalog == null || !this.orderCatalog.equals(other.orderCatalog))) {
            return false;
        }
        return true;
    }
    
    public void addOrder(Order o){
        Order order = new Order();
        order = o;
        orderCatalog.add(o);
    }
    
}
