/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author abhishekashwathnarayanvenkat
 */
public class ProductCatalog {
    
    private static ProductCatalog productCatalog;
    private ArrayList<Product> al;

    private ProductCatalog() {
        al = new ArrayList<>();
    }
    
    public static ProductCatalog getInstance(){
        if(productCatalog != null) return productCatalog;
        else return new ProductCatalog();
    }
    
    public List<Product> getProductCatalog(){
        return al;
    }
    
    
    public Product addProduct(){
        Product p = new Product();
        al.add(p);
        return p;
    }
    
    public void removeProduct(Product p){
        al.remove(p);
    }
    
    public Product searchProduct(int id){
        for (Product product : productCatalog.getProductCatalog()) {
            if(product.getModelNumber()==id){
                return product;
            }
        }
        return null;
    }
}
