 /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Business;

/**
 *
 * @author Bala
 */
public class ConfigureBusiness {
    public static Business initializeBusiness(){
        Business business = Business.getInstance();
        UserAccount userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.setFirstName("Admin");
        userAccount.setLastName("Admin");
        userAccount.setOrganization("NEU");
        userAccount.setUserName("admin");
        userAccount.setPassword("admin");
        userAccount.setRole(UserAccount.ADMIN_ROLE);
        userAccount.setIsActive(true);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.setFirstName("Nan");
        userAccount.setLastName("Deng");
        userAccount.setOrganization("Xerox");
        userAccount.setUserName("dn");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.EMPLOYEE_ROLE);
        userAccount.setIsActive(true);
        
        userAccount = business.getUserAccountDirectory().addUserAccount();        
        userAccount.setFirstName("Kal");
        userAccount.setLastName("Bug");
        userAccount.setOrganization("NEU");
        userAccount.setUserName("kb");
        userAccount.setPassword("1234");
        userAccount.setRole(UserAccount.CUSTOMER_ROLE);
        userAccount.setIsActive(true);
                
        return business;
    }
}
