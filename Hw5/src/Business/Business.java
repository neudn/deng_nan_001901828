/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Business {
    
    private static Business business;
    private UserAccountDirectory userAccountDirectory;
    private ProductCatalog productCatalog;
    private CustomerDirectory customerDirectory;
    private MasterOrderCatalog masterOrderCatalog;
    
    private Business(){
        userAccountDirectory = new UserAccountDirectory();
        productCatalog = ProductCatalog.getInstance();
        customerDirectory = new CustomerDirectory();
        masterOrderCatalog = new MasterOrderCatalog();
    }
    
    public static Business getInstance(){           // Singleton Pattern 
        if(business != null){                        // make sure only one is initialized
            return business;
        }
        return new Business();
    }

    public CustomerDirectory getCustomerDirectory() {
        return customerDirectory;
    }

    public void setCustomerDirectory(CustomerDirectory customerDirectory) {
        this.customerDirectory = customerDirectory;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public void setUserAccountDirectory(UserAccountDirectory userAccountDirectory) {
        this.userAccountDirectory = userAccountDirectory;
    }

    public ProductCatalog getProductCatalog() {
        return productCatalog;
    }

    public void setProductCatalog(ProductCatalog productCatalog) {
        this.productCatalog = productCatalog;
    }

    public MasterOrderCatalog getMasterOrderCatalog() {
        return masterOrderCatalog;
    }

    public void setMasterOrderCatalog(MasterOrderCatalog masterOrderCatalog) {
        this.masterOrderCatalog = masterOrderCatalog;
    }
 
    
}
