/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class Order {
    private ArrayList<OrderItem> orderList;
    private int orderNumber;
    private static int count = 0;
    private int commision;
    private int cost;
    private int temPrice;
    private String status;
    private String customerName;
    private String salespersonName;
    
    
    public Order(){
        orderList = new ArrayList<OrderItem>();
        count++;
        orderNumber = count;
        cost = 0;
    }
    
    public OrderItem addOrderItem(Product p, int q, int price){
        OrderItem o = new OrderItem();
        o.setProduct(p);
        o.setQuantity(q);
        orderList.add(o);
        return o;
    }
    
    public void removeOrderItem(OrderItem o){
        orderList.remove(o);
    }

    public ArrayList<OrderItem> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<OrderItem> orderList) {
        this.orderList = orderList;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public int getPrice() {
        return temPrice;
    }

    public void setPrice(int price) {
        this.temPrice = price;
    }
    
    public int calcCost(){
        cost = 0;
        for(OrderItem oi: this.getOrderList()){
            cost += oi.getQuantity()*oi.getProduct().getFloorPrice();
        }
        return cost;
    }
    
    public double calcCommission(){
        return (temPrice - calcCost())*0.1;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public int getTemPrice() {
        return temPrice;
    }

    public void setTemPrice(int temPrice) {
        this.temPrice = temPrice;
    }

    public String getSalespersonName() {
        return salespersonName;
    }

    public void setSalespersonName(String salespersonName) {
        this.salespersonName = salespersonName;
    }
    
    
}
