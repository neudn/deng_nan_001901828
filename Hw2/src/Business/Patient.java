/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class Patient {
    private String name;
    private String ID;
    private int age;
    private String DocName;
    private String preferredPharmacy;
    // vital sign history
    private VitalSignHistory vsh;
    
    public Patient(){
        vsh = new VitalSignHistory();
    }
    
    public String getName(){
        return name;
    }
    public void setName(String n){
        this.name = n;
    }
    public String getID(){
        return ID;
    }
    public void setID(String id){
        this.ID = id;
    }
    public int getAge(){
        return age;
    }
    public void setAge(int a){
        this.age = a;
    }
    public String getDocName(){
        return DocName;
    }
    public void setDocName(String dn){
        this.DocName = dn;
    }
    public String getPrefPharm(){
        return preferredPharmacy;
    }
    public void setPrefPharm(String pp){
        this.preferredPharmacy = pp;
    }
    
    public VitalSignHistory getVitalSignHistory(){
        return vsh;
    }
}


