/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class VitalSign {
    private int bloodPressure;
    private int heartRate;
    private double weight;
    private double respiratoryRate;
    private String date;
    
    
    public int getBloodPressure(){
        return bloodPressure;
    }
    
    public void setBloodPressure(int bp){
        this.bloodPressure = bp;
    }
    
    public int getHeartRate(){
        return heartRate;
    }
    
    public void setHeartRate(int hr){
        this.heartRate = hr;
    }
    
    public double getWeight(){
        return weight;
    }
    
    public void setWeight(double wei){
        this.weight = wei;
    }
    
    public double getRespiratoryRate(){
        return respiratoryRate;
    }
    
    public void setRespiratoryRate(double rr){
        this.respiratoryRate = rr;
    }
    
    public String getDate(){
        return date;
    }
    
    public void setDate(String dat){
        this.date = dat;
    }
    
    @Override
    public String toString(){
        return this.getDate();
    }
 
    
    public String isNormal(VitalSign vs, int age){
        if(age >= 1 && age <= 3 
         &&vs.getHeartRate() >= 80 && vs.getHeartRate() <= 130
         &&vs.getRespiratoryRate() >= 20 && vs.getRespiratoryRate() <= 30
         &&vs.getBloodPressure() >= 80 && vs.getBloodPressure() <= 110
         &&vs.getWeight() >= 22 && vs.getWeight() <= 31    
        ) return "Normal";
        
        if(age >= 4 && age <= 5 
         &&vs.getHeartRate() >= 80 && vs.getHeartRate() <= 120
         &&vs.getRespiratoryRate() >= 20 && vs.getRespiratoryRate() <= 30
         &&vs.getBloodPressure() >= 80 && vs.getBloodPressure() <= 110
         &&vs.getWeight() >= 31 && vs.getWeight() <= 40   
        ) return "Normal";
        
        if(age >= 6 && age <= 12 
         &&vs.getHeartRate() >= 70 && vs.getHeartRate() <= 110
         &&vs.getRespiratoryRate() >= 20 && vs.getRespiratoryRate() <= 30
         &&vs.getBloodPressure() >= 80 && vs.getBloodPressure() <= 120
         &&vs.getWeight() >= 41 && vs.getWeight() <= 92   
        ) return "Normal";
        
        if(age >= 13 
         &&vs.getHeartRate() >= 55 && vs.getHeartRate() <= 105
         &&vs.getRespiratoryRate() >= 12 && vs.getRespiratoryRate() <= 20
         &&vs.getBloodPressure() >= 110 && vs.getBloodPressure() <= 120
         &&vs.getWeight() >= 110  
        ) return "Normal";
        
        return "Abnormal";
        
    }
}
