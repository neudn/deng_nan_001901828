/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;
import java.util.ArrayList;

/**
 *
 * @author dn
 */
public class VitalSignHistory {
    private ArrayList<VitalSign> vsh;
    
    public VitalSignHistory(){
        vsh = new ArrayList<>();
    }
    
    public ArrayList<VitalSign> getVitalSignHistory(){
        return vsh;
    }
    
    public VitalSign addVitalSign(){
        VitalSign vs = new VitalSign();
        vsh.add(vs);
        return vs;
    }
    
    public void deleteVitalSign(VitalSign vs){
        vsh.remove(vs);
    }
    
}
