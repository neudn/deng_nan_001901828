/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UI;
import Business.*;

/**
 *
 * @author dn
 */
public class MainJFrame extends javax.swing.JFrame {
    
    private Patient p;

    /**
     * Creates new form MainJFrame
     */
    public MainJFrame() {
        initComponents();
        p = new Patient();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        CreatePatient = new javax.swing.JButton();
        ViewPatient = new javax.swing.JButton();
        AddVitalSign = new javax.swing.JButton();
        ViewVitalSign = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        CreatePatient.setText("CreatePatient");
        CreatePatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreatePatientActionPerformed(evt);
            }
        });

        ViewPatient.setText("ViewPatient");
        ViewPatient.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewPatientActionPerformed(evt);
            }
        });

        AddVitalSign.setText("Add Vital Sign");
        AddVitalSign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddVitalSignActionPerformed(evt);
            }
        });

        ViewVitalSign.setText("View VSH");
        ViewVitalSign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ViewVitalSignActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ViewPatient, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(AddVitalSign, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(CreatePatient)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(ViewVitalSign, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(CreatePatient)
                .addGap(31, 31, 31)
                .addComponent(ViewPatient)
                .addGap(33, 33, 33)
                .addComponent(AddVitalSign)
                .addGap(33, 33, 33)
                .addComponent(ViewVitalSign)
                .addContainerGap(64, Short.MAX_VALUE))
        );

        jSplitPane1.setLeftComponent(jPanel1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 395, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 298, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void CreatePatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreatePatientActionPerformed
        // TODO add your handling code here:
        CreatePatient createPatient = new CreatePatient(p);
        jSplitPane1.setRightComponent(createPatient);
    }//GEN-LAST:event_CreatePatientActionPerformed

    private void ViewPatientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewPatientActionPerformed
        // TODO add your handling code here:
        ViewPatient viewPatient = new ViewPatient(p);
        jSplitPane1.setRightComponent(viewPatient);
    }//GEN-LAST:event_ViewPatientActionPerformed

    private void ViewVitalSignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ViewVitalSignActionPerformed
        // TODO add your handling code here:
        ViewVitalSignHistory viewVSH = new ViewVitalSignHistory(p);
        jSplitPane1.setRightComponent(viewVSH);
    }//GEN-LAST:event_ViewVitalSignActionPerformed

    private void AddVitalSignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddVitalSignActionPerformed
        // TODO add your handling code here:
        AddVitalSign addVitalSign = new AddVitalSign(p);
        jSplitPane1.setRightComponent(addVitalSign);
    }//GEN-LAST:event_AddVitalSignActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainJFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddVitalSign;
    private javax.swing.JButton CreatePatient;
    private javax.swing.JButton ViewPatient;
    private javax.swing.JButton ViewVitalSign;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSplitPane jSplitPane1;
    // End of variables declaration//GEN-END:variables
}
