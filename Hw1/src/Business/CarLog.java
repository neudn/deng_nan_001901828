/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author dn
 */
public class CarLog {
    private String carMake;
    private String carModel;
    private String miles;
    private String batteryStatus;
    private String engineStatus;
    private String oilStatus;
    private String serviceDate;
    private String serviceDescription;
    private String customerName;
    private String customerPhoneNumber;
    private String email;
    
    public String getCarMake() {
        return carMake;
    }
    public void setCarMake(String carMake) {
        this.carMake = carMake;
    }  
    public String getCarModel() {
        return carModel;
    }
    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }
    public String getMiles() {
        return miles;
    }
    public void setMiles(String miles) {
        this.miles = miles;
    }
    public String getBatteryStatus() {
        return batteryStatus;
    }
    public void setBatteryStatus(String batteryStatus) {
        this.batteryStatus = batteryStatus;
    }
    public String getEngineStatus() {
        return engineStatus;
    }
    public void setEngineStatus(String engineStatus) {
        this.engineStatus = engineStatus;
    }
    public String getOilStatus() {
        return oilStatus;
    }
    public void setOilStatus(String oilStatus) {
        this.oilStatus = oilStatus;
    }
    public String getServiceDate() {
        return serviceDate;
    }
    public void setServiceDate(String serviceDate) {
        this.serviceDate = serviceDate;
    }
    public String getServiceDescription() {
        return serviceDescription;
    }
    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }
    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
}